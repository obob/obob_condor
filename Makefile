# simple makefile to simplify repetetive build env management tasks under posix

# caution: testing won't work on windows, see README
VERSION_FILE:=VERSION
VERSION:=$(strip $(shell cat ${VERSION_FILE}))
PYPIVERSION:=$(subst _,.,$(VERSION))
pypidist:=dist/obob_condor-$(PYPIVERSION).tar.gz

ifeq ($(findstring dev,$(VERSION)), dev)
	export TWINE_REPOSITORY_URL=https://test.pypi.org/legacy/
	ifeq ($(shell echo -n $(PYPIVERSION) | tail -c 1), v)
		PYPIVERSION:=$(PYPIVERSION)0
	endif
	ISDEV:=1
else
	ISDEV:=0
endif

update_deps:
	pip install -U -r requirements.txt

build-doc:
	cd doc; make clean; make html

autobuild-doc:
	sphinx-autobuild doc/source doc/build

clean-dist:
	rm -rf dist
	rm -rf obob_condor.egg-info

$(pypidist):
	python setup.py sdist

make-dist: $(pypidist)

upload-dist: make-dist
	twine upload dist/obob_condor-$(PYPIVERSION).tar.gz
ifneq ($(findstring dev,$(VERSION)), dev)
	git tag -a v$(VERSION) -m "version $(VERSION)"
	git push origin v$(VERSION)
endif
