# -*- coding: UTF-8 -*-
# Copyright (c) 2018, Thomas Hartmann
#
# This file is part of the obob_condor Project, see: https://gitlab.com/obob/obob_condor
#
#    obob_condor is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_condor is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import obob_condor

class JobWithArgs(obob_condor.Job):
    def run(self, my_arg):
        print(my_arg)

class JobWithThreeArgs(JobWithArgs):
    def run(self, first, second, third):
        print('first: %s\n'
              'second: %s\n'
              'third: %s' % (str(first), str(second), str(third)))


class JobChangesArgs(obob_condor.Job):
    def run(self, arg_list, kwarg_list):
        arg_list.append('hi')
        kwarg_list.append('hello')


class JobWithAutoFilename(obob_condor.AutomaticFilenameJob):
    base_data_folder = '/base/'
    job_data_folder = 'job'
    include_hash_in_fname = True
    exclude_kwargs_from_filename = ['excluded_arg']
    create_folders = False

    def run(self, subject_id, included_arg, excluded_arg):
        print('hi')
