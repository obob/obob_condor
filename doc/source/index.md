% obob_condor documentation master file, created by
% sphinx-quickstart on Mon Mar 26 16:49:50 2018.
% You can adapt this file completely to your liking, but it should at least
% contain the root `toctree` directive.

# Welcome to obob_condor's documentation!

## What is this?

You were probably directed to this python package / website because you are going to
use the cluster of the group of Prof. Weisz at the University of Salzburg.

Lots of the analyses we run uses lots of RAM and/or takes a long time to compute
on a workstation or laptop. A cluster (in general, not specifically this one)
provides access to powerful computing resources that are shared between users.

In order to ensure a fair allocation of the resources, you cannot access them
directly but instead define so-called {class}`obob_condor.Job` together with how much RAM and how
many CPUs your job is going to need and submit that information to the cluster.

The cluster then tries to allocate these resources for you and runs your job.

The software we use here is called [HTCondor](https://htcondor.readthedocs.io/).
It is a very powerful and complex piece of software. This package makes it much
easier for you to use the cluster by hiding a lot of the complexities.

If you are already familiar with our cluster infrastructure (because you
are migrating from Matlab), you can skip the first section and go directly to 
{doc}`first_steps`. Otherwise, get {doc}`htcondor_intro`.

```{toctree}
:caption: Contents
:maxdepth: 2

htcondor_intro
create_environment
first_steps
autofilename
reference
```

# Indices and tables

- {ref}`genindex`
- {ref}`modindex`
- {ref}`search`
