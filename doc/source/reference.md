# Reference

```{eval-rst}
.. automodule:: obob_condor

   .. autoclass:: JobCluster
      :members:

   .. autoclass:: Job
      :members:

   .. autoclass:: AutomaticFilenameJob
      :show-inheritance:
      :members:

   .. autoclass:: PermuteArgument
```
