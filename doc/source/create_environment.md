# How to create a python environment for you cluster jobs

## Create a new python environment and make sure `obob_condor` gets installed in it

Open a terminal and navigate to your folder on `/mnt/obob`:

```bash
cd /mnt/obob/staff/thartmann
```

Now create a new folder where you want to store the code and the environment:

```bash
mkdir my_obob_condor_test
cd my_obob_condor_test
```

Create a file called `environment.yml`. This file lists all the conda and pypi
packages your project needs. Take a look [here for a description](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-file-manually). For this tutorial, it should look
like this:

```yaml
channels:
  - defaults
  - conda-forge

dependencies:
  - pip
  - spyder
  - pip:
      - obob_condor
```

You can use any text editor you like,
but if you are connected via X2go or remote desktop, the easiest way is to start
a nice graphical text editor like this:

```bash
gedit environment.yml
```

If you wanted to analyze actual data with it, you would probably need to add `mne`, `pandas`, etc. to it. But we can leave it like this.

Copy-paste the content in there, save and exit and go back to the terminal.

You can now create the environment like this:

```bash
mamba env create -p ./.venv
```

When this is done, you must activate the environment:

```bash
conda activate ./.venv
```

And then start Spyder (a Python editor) like this:

```bash
spyder
```

Before you go on, make sure that:

1. The folder that is shown in the box on the top right, just below the menu bar
   is the folder you just created. If not, click the folder button to the
   right and navigate to the correct one.
2. Just below, you find a pane which has four different tab options: "Help",
   "Variable Explorer", "Plots" and "Files". Make sure "File" is selected.

You should now see the `environment.yml` file that we created before.