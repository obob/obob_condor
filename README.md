# obob_condor
A python module to make it simple to submit jobs to the OBOB cluster directly from python

## How to use
coming up...

## Documentation
coming up....

## Code
The source code can be found here: <https://gitlab.com/obob/obob_condor>

## License
This module is developed by Thomas Hartmann at the Universität Salzburg. You are free to use, copy, modify, distribute it under the terms of the GPL3.
